\documentclass[main.tex]{subfiles}

\begin{document}
The basic structure of our GP algorithm was adapted from the algorithm
presented in Chapter~7 of the book \emph{Evolutionary Computation},
by Back et al.\cite{back00}
The algorithm they present takes a population of fixed-size and evolves it
as follows.

First, a randomized population of size $n$ is generated and fitness is
evaluated and assigned to each member.
Then, for $g$ generations, a subpopulation of size $m<n$ is selected and
has crossover and mutation applied to return the population to size $n$.
The 1:1 replacement ratio of this algorithm requires the selection pressure
to have some measure of stochasticity and to select the same parent
members multiple times.\footnote{%
Using a non-stochastic selection method will result in no selection,
as the operator selects would be selecting $n$ individuals from a pool of $n$.}
The new generation then is tested for fitness and the process repeats
until done.

A summary of the modified algorithm we used can be seen in pseudocode in
Listing~\ref{lst:main} below.
In Step~1, an initial population of $n$ program trees with a maximum depth
of three is randomly generated by DEAP to represent the player strategies.
In Step~2, the main loop of our simulation is entered, where it stays for
$g$ generations, which is divided into three principal sub-steps:
Step~2.1, wherein player strategies compete against each other to win
chips and assess fitness;
Step~2.2, wherein DEAP is used to select a parent population from the
total population and to generate new player strategies with a maximum
depth of six by applying mutation and crossover on members of the
selected parent population;
and Step~2.3, wherein a selection of the best ten strategies is archived
every hundred generations for comparison and analysis.

\lstset{basicstyle=\normalsize\ttfamily,
        caption={Main Algorithm Overview},
        label=lst:main}
\begin{lstlisting}
Algorithm (Input: n - Number of Players,
                  g - Number of Generations,
                  h - Max Number of Hands Played,
                  c - Number of Starting Chips):

    Step 1: Generate n Players
        An initial population of n random
        trees with a maximum depth of six
        is generated by DEAP at the
        start of the simulation.

    Step 2: Evolve Poker for g Generations
      Step 2.1: Play Poker
        The population is randomly divided
        into groups of ten players,
        which compete in a game against
        each other.

        Players begin with c chips, and
        continue playing until one player
        holds all the chips, or h hands
        have been played.

        Repeat this process five times and
        average the amount of chips held at
        the end of each game by each player
        to be stored as fitness.

      Step 2.2: Generate New Players
        From the total population, select
        a parent population of size n.

        Apply mutation and recombination to
        the parent population. This population
        becomes the next generation.

    Loop Until Done:
        This process is repeated until g
        generations have been completed.
\end{lstlisting}

Since there are many layers to our implementation, and many looping cycles
that occur in a game of poker, for clarity, within this paper,
a \emph{circuit} refers to a single betting cycle including all remaining
players within one dealt hand;
a \emph{game} refers to a single set of dealt hands of poker between
one group of players;
a \emph{round} refers to one iteration of the set of games played by
all members of the population within Step~2.1 of our algorithm;
and a \emph{tournament} refers to the three consecutive rounds played for
fitness determination within one iteration of Step~2.1 of our algorithm.

The following subsection contains a brief explanation for the parameters
we selected for our GP implementation.
In the subsections following that, each of the steps above is explained
further in much greater detail, followed by a description of the
experiments that were ran to test the model.

\subsection{Parameter Selection}
Since the underlying evolutionary algorithm is a symbolic regression, there
are numerous parameters that needed to be selected to configure the kinds
of program trees that DEAP should generate, outlined in Table~\ref{tab:prm}.
The majority of operations our implementation generates are Boolean
operations: if..then, comparison, and the Boolean logical operators: and,
or, exclusive-or, and not;
we chose these because the strategies we generate are primarily
rules-based decision-making strategies dependent on game state,
which can be described as a series of Boolean checks.
For arithmetic operations, we used addition, subtraction, multiplication, and protected floor division.

The constant terminal values that are used in tree generation are the
constant Boolean values true and false, and integers in the range
negative ten through ten.
This range of integer values was chosen because the range of values for
rank of playing cards are 1--13, which means the random integers we
generate will be on around the same order of magnitude, which could help
in generating some of the rules-based strategies we hope to evolve.

The variable terminals that are used include the rank and suit of visible
cards ($r_1, r_2, \cdots, r_7$ and $s_1, s_2, \cdots, s_7$), the amount of chips bet by each
player so far in the current circuit ($b_1, \cdots, b_n$),
the total number of chips each player has at evaluation time in
the circuit ($t_1, \cdots, t_n$), and the total pot size in chips ($t_0$).
The value of unseen cards was set to zero, to minimize their effect on
the program tree.

The probability of crossover was set to the DEAP default recommended value
of fifty percent, and the probability of mutation was also set to the
DEAP default recommended value of ten percent.
The maximum program tree depth to be generated by these methods was set to
six to help minimize potential code bloat and to keep the amount time
spent executing program trees low.

\begin{table}
\caption{DEAP GP Implementation Parameters}
\begin{tabular}{l|l}
Parameter & Value(s)	\\
\hline
Boolean Operators & if..then, $<$, $\le$, $>$, $\ge$, =,
                    and, or, xor, not \\
Arithmetic Operators & +, -, *, protected floor / \\
Constant Terminals & True, False, Integers $\in [-10, 10]$ \\
Variable Terminals & Cards Visible, Player Bets, Player Chips \\
                   & Total Pot Size \\
Probability of Crossover & 50\% \\
Probability of Mutation & 10\% \\
Probability of Survival & 40\% \\
Max Depth & 6 \\
\end{tabular}
\label{tab:prm}
\end{table}

\subsection{Initial Population Generation}
In order to begin our algorithm, a initial population of program trees
must be generated.
We randomly generate these trees using the the ramped half and half method. The trees are all limited to a depth of six.
The size of our generated initial population is a fixed size $n$, which
is maintained throughout the run of the algorithm.

These strategies are all equations. While the individual is playing poker, the equation is evaluated at every turn. The output of the equation determines the action that the individual will take on that turn. A positive output result in a raise by the output amount. An output of 0 results in calling. When a strategy function returns a negative number, this is considered a fold. The poker simulation prevents strategies from betting more chips than they currently have on hand.

\subsection{Playing Poker and Determining Fitness}
To assess the fitness of individual strategies within our simulation, strategies play poker in a groups of ten. In such a group of ten, each strategy is given 500 chips and then the strategies iterate through
a game until one player has amassed all the chips currently in play,
or 1000 hands have been played.
For each hand, a standard fifty-two card deck is dealt to the players,
which is shuffled between each hand.
When the game ends, the net winnings
of each individual is record.
The net winnings are recorded as the ratio of ending chips to starting chips. Thus, if the net winnings equals 1 means the strategy ended with the same amount of money it started with. If the net winnings is less the 1, the strategy lost money and if the net winnings is greater than 1, the strategy won money.

To fairly compute fitness, a consistent population of 20 random strategies was created to fairly determine the success of each individual in the population. To compute the fitness of an individual, the individual is temporarily added to the population of consistent strategies. Next, the individual is put into groups without repeated strategies (2 groups since 20 only allows 2 unique groups of 9). Each group plays 1000 hands, as described above, and the net winnings are recorded. This is repeated 5 times with new groups of strategies. The average net winnings of the 10 iterations of 1000 hands is the fitness for the individual. This entire process must be repeated for each individual in the population.

This fitness function is computationally expensive since there are 10 simulation of 1000 hands for all 100 individuals every generation. Initially, we considered breaking the population into groups and having them play against each other for one iteration of 100 hands. Simultaneously, individuals where selected as parents, if they won the game. Due to the inconsistency of the players the fitness (net winnings), it was impossible to determine if the strategies where improving over the generations. Figure~\ref{fig:fast_fitness} shows the use of this fitness function over 50 generations. In the first few generations, there is at least one individual that is able to score the maximum possible score of 10, but drops off steeply. This could be due a new strategy that is competing with the high scoring strategy or a game with unlucky cards. In either case, this gives an idea for the difficulty of using this fitness function.

	Figure~\ref{fig:fast_fitness2} shows the data from the same individuals in Figure~\ref{fig:fast_fitness}, but this time the slow fitness function is used to compute the fitness. The fitness is almost never above 1. We would hope to see improvement as the generations increase, but the fitnesses remain essentially constant. For these reasons, the results where created using the slow fitness function.

\begin{figure}
	\centering
    \includegraphics[scale=0.5]{fast_fitness.png}
    \caption{Fitness over 50 Generations Using Fast Fitness Function}
    \label{fig:fast_fitness}
\end{figure}

\begin{figure}
	\centering
    \includegraphics[scale=0.5]{slow_fast_fitness.png}
    \caption{Fitness Values Computed by the Slow Fitness Function but Evolved with the Fast Fitness Function}
	\label{fig:fast_fitness2}
\end{figure}

\subsection{New Player Generation}
After completing the simulated poker tournament and assessing fitness, tournament selection is used to select a new population of the same size. The mutation and crossover operation are used on this population and becomes the population for the next generation. The probability that an individual that has been selected undergoes mutation is 10\%. The probability that an individual will crossover with another member of the parent population is 50\%. There is also some elitism in our algorithm, as the individuals which are selected to be parents but do not have crossover or mutation operation applied will move onto the next generation unchanged.

\subsection{Experiment Design}
In order to test our algorithm we ran an implementation of our algorithm multiple
times. % HOW MANY
A brief overview of the parameters we used for running our experiments can
be seen in Table~\ref{tab:params}.

\begin{table}
\caption{Poker Simulation Parameters}
\begin{tabular}{l|l}
Parameter & Value	\\
\hline
Population Size, $n$ & 100 \\
Game Size & 10 \\
Rounds of Play & 5 \\
Number of Generations, $g$ & 50 \\
Maximum Hands Played, $h$ & 1000 \\
Initial Number of Chips, $c$ & 500 \\
\end{tabular}
\label{tab:params}
\end{table}

We used a population size of one hundred individuals, to ensure the problem
was not too time intensive to run.
The games in the tournament had ten strategies playing at a time because
ten players is the number of players per game in traditional human tournament
Texas Hold'em poker play.
We chose to run the tournaments in the algorithm for five rounds, with maximum
number of hands of one thousand, and with an initial number of chips
of five hundred, again to try to save on the computational complexity of our
algorithm, but also, with enough time that the weak strategies of the early
algorithm should be able to differentiate and diversify into different fitness 
strains within a few rounds of play.

As the algorithm was ran the fitness of the best of the population
was recorded.
We had two separate fitness testing functions: the fitness function that was used in the algorithm, which took the relative fitness of individuals compared to the random members at their tables, and a slower fitness function that tried to calculate a more objective fitness value of the strategies against a group
of randomly generated strategies.

\end{document}

